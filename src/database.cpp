//
// Created by kenenbek on 30.03.17.
//

#include <SQLiteCpp/Database.h>
#include "myfunc_list.h"
#include <string>
#include <vector>
#include <algorithm>
#include <sqlite3.h>
#include "database.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(database, "messages specific for database");

using namespace std;

#ifdef SQLITECPP_ENABLE_ASSERT_HANDLER
namespace SQLite
{
/// definition of the assertion handler enabled when SQLITECPP_ENABLE_ASSERT_HANDLER is defined in the project (CMakeList.txt)
void assertion_failed(const char* apFile, const long apLine, const char* apFunc, const char* apExpr, const char* apMsg)
{
    // Print a message to the standard error output stream, and abort the program.
    std::cerr << apFile << ":" << apLine << ":" << " error: assertion failed (" << apExpr << ") in " << apFunc << "() with message \"" << apMsg << "\"\n";
    std::abort();
}
}
#endif


JobDatabase::JobDatabase() :
        mDb(jobs_file, SQLite::OPEN_READWRITE),
        mQuery(mDb, "SELECT * FROM jobs WHERE SubmissionTime < :TIME AND Site == :HOST_NAME LIMIT :JOBS_AMOUNT"),
        deleteQuery(mDb, "DELETE FROM jobs "
                                "WHERE id in "
                                "("
                                "SELECT id FROM jobs WHERE SubmissionTime < :TIME AND Site == :HOST_NAME LIMIT :JOBS_AMOUNT"
                                ")")
{

}

JobDatabase::~JobDatabase() {

}


/// List the rows where the "weight" column is greater than the provided aParamValue
std::vector<Job*>* JobDatabase::GetJobs (std::string JOBS_AMOUNT, std::string HOST_NAME, FileDatabase& FD)
{
    /**
    @type function
    Match jobs according to historical base.
    (Because we know where job will be running and when it will be scheduled)

    Parameters:
    -----------
    @JOBS_AMOUNT -- how many jobs tier requested
    @HOST_NAME -- name of tier which requested a job batch
    @return job batch
    */
    const double GiGa = 1000000000;
    double CURRENT_TIME = MSG_get_clock() + 1490821200;
    std::vector<Job*>* jobBatch = new std::vector<Job*>;

    mQuery.bind(":HOST_NAME", HOST_NAME);
    mQuery.bind(":JOBS_AMOUNT", JOBS_AMOUNT);
    mQuery.bind(":TIME", CURRENT_TIME);
    deleteQuery.bind(":HOST_NAME", HOST_NAME);
    deleteQuery.bind(":JOBS_AMOUNT", JOBS_AMOUNT);
    deleteQuery.bind(":TIME", CURRENT_TIME);
    //XBT_INFO("%f", MSG_get_clock());

    while (mQuery.executeStep())
    {
        Job* job = new Job;
        job->JobId = mQuery.getColumn("id").getInt();
        job->SubmissionTime = mQuery.getColumn("SubmissionTime").getInt() - 1490821200;
        job->startSchedulClock = MSG_get_clock();
        job->Federation = mQuery.getColumn("Site").getString();
        job->JobType = mQuery.getColumn("JobType").getString();
        job->TotalCPUTime = mQuery.getColumn("TotalCPUTime").getDouble() * GiGa;

        std::string x = mQuery.getColumn("InputFiles").getString();
        std::string y = mQuery.getColumn("OutputFiles").getString();

        job->InputFiles = split(x, "', '");
        job->OutputFiles = split(y, "', '");

        std::string CONDITION = prepare_condition(job->InputFiles, job->OutputFiles);

        job->files_map = FD.GetDataInfo(CONDITION);

        jobBatch->push_back(job);
    }

    // DELETE from queue
    deleteQuery.exec();

    mQuery.reset();
    deleteQuery.reset();
    return jobBatch;
}

int JobDatabase::GetSize() {
    const char* STATEMENT = "SELECT COUNT(*) FROM jobs";
    return atoi(mDb.execAndGet(STATEMENT));
}

std::string prepare_condition(const std::vector<std::string>& a, const std::vector<std::string>& b)
{
    /**
        Prepare condition for query to BKK.
        Example:
            id in ('Storage1', 'Storage2')
     */
    std::string new_string = "";
    for (auto& s : a) {
        new_string += "'";
        new_string += s;
        new_string += "', ";
    }
    for (auto& ss : b) {
        new_string += "'";
        new_string += ss;
        new_string += "', ";
    }
    if (new_string.size() < 1)
        return "id in ("");";
    new_string.pop_back(); new_string.pop_back();
    return "(" + new_string + ");";
}

std::string create_CONDITION(std::vector<std::string> in, std::vector<std::string> out){
    std::stringstream result;
    in.insert(in.end(), out.begin(), out.end());
    std::copy(in.begin(), in.end(), std::ostream_iterator<std::string>(result, "\", \""));

    std::string x = result.str();
    x.erase(x.length()-3, x.length());
    std::string CONDITION = "WHERE id in (\"" + x + ");";
    return CONDITION;
}


FileDatabase::FileDatabase() {
    int rc = sqlite3_open(input_files_file.c_str(), &mDb);
    if( rc ){
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(mDb));
    }
}

FileDatabase::~FileDatabase()
{
    sqlite3_close(mDb);
}


std::map<std::string, InputFile> * FileDatabase::GetDataInfo(std::string &CONDITION) {
    int rc;

    char* SQL_part_1 = "SELECT * FROM bkk WHERE id in"; // has length < 40
    char SQL_FULL[CONDITION.size() + 40];
    sprintf(SQL_FULL, "%s %s", SQL_part_1, CONDITION.c_str());

    std::map<std::string, InputFile>* files_map = new std::map<std::string, InputFile>;

    sqlite3_stmt *stmt = NULL;
    rc = sqlite3_prepare_v2(mDb, SQL_FULL, -1, &stmt, NULL);
    if (rc != SQLITE_OK)
        fprintf(stderr, SQL_FULL);

    rc = sqlite3_step(stmt);
    while (rc != SQLITE_DONE && rc != SQLITE_OK)
    {
        InputFile file_info;
        std::string id = (char*) sqlite3_column_text(stmt, 0);
        file_info.Size = sqlite3_column_double(stmt, 1);
        std::string x = (char*) sqlite3_column_text(stmt, 2);

        file_info.Storages = split(x, "\', \'");;
        files_map->insert(std::make_pair(id, file_info));
        rc = sqlite3_step(stmt);
    }

    rc = sqlite3_finalize(stmt);

    return files_map;

}

std::vector<std::string> split(const std::string &text, char* sep) {
    if (text.compare("[]") == 0)
        return std::vector<std::string>();
    std::string new_string = text.substr(2, text.size() - 4);
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = new_string.find(sep, start)) != std::string::npos) {
        tokens.push_back(new_string.substr(start, end - start));
        start = end + 4;
    }
    tokens.push_back(new_string.substr(start));
    return tokens;
}


template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss;

    try {
        ss.str(s);
    }catch (std::exception e){
        std::cout << "Enen sigen" << e.what() << std::endl;
        std::cout << s << std::endl;
        std::cout << "Enen sigen" << e.what() << std::endl;
    }

    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
    ss.clear();
}
