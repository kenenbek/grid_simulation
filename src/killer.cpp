//
// Created by ken on 09.10.16.
//

#include <simgrid/msg.h>
#include <string>
#include "myfunc_list.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(killer, "messages specific for killer");

int killer(int argc, char* argv[]){
	/**
		@type simgrid process
		Kills all proccesses on tiers at the end of simulation

        Simgrid process parameters:
        --------------------------
        None
	*/

    const double timeout = 1000;
    msg_task_t task = NULL;
    msg_task_t scheduler_task = NULL;

    while (TRUE){
        if (JOB_QUEUE_SIZE <= 0){
            MSG_process_sleep(100.);

            xbt_dynar_t dynar = MSG_hosts_as_dynar();
            msg_host_t host;
            unsigned int cursor;
            xbt_dynar_foreach(dynar, cursor, host){
                task = MSG_task_create("finalize", 0, MESSAGES_SIZE, NULL);
                MSG_task_dsend(task, MSG_host_get_name(host), xbt_free_f);

            }

            scheduler_task = MSG_task_create("finalize", 0, MESSAGES_SIZE, NULL);
            MSG_task_send(scheduler_task, "scheduler");
            xbt_dynar_free(&dynar);
            break;
        }
        MSG_process_sleep(timeout);
    }
    return 0;
}

int die_or_not(){
	/**
		@type function
		Checks amount of jobs in the queue.
		If queue is empty --> simulation should be killed. 
	*/
    MSG_sem_acquire(sem_requester);
    if (JOB_QUEUE_SIZE <= 0){
        MSG_process_kill(MSG_process_self());
        MSG_sem_release(sem_requester);
    }
    MSG_sem_release(sem_requester);
    return 0;
}
