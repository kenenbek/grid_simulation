//
// Created by kenenbek on 19.04.17.
//
#include <simgrid/msg.h>

#ifndef CSIM2SIM_MY_MSG_H
#define CSIM2SIM_MY_MSG_H


msg_file_t MY_MSG_file_open(const std::string& path);

msg_error_t MY_MSG_file_rcopy_and_close(msg_file_t& file, const std::string& dest_path, const std::string& dest_storage_name);

#endif //CSIM2SIM_MY_MSG_H
