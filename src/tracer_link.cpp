//
// Created by ken on 05.08.16.
//
#include <simgrid/msg.h>
#include "myfunc_list.h"


int tracer_traffic(const std::string& src_disk, const std::string& dst_disk, double size){
	/**
	   @type function
		Adds @size bytes to link's cumulative sum of transferred data between
		@dst and @src. 
		Writes value to trace file. 		
	*/
    MSG_sem_acquire(sem_link);

    msg_storage_t src_storage = MSG_storage_get_by_name(src_disk.c_str());
    msg_storage_t dst_storage = MSG_storage_get_by_name(dst_disk.c_str());

    const char* src = MSG_storage_get_host(src_storage);
    const char* dst = MSG_storage_get_host(dst_storage);


    TRACE_link_srcdst_variable_add(src, dst, "traffic", size);
    MSG_sem_release(sem_link);
    return 0;
}
