//
// Created by kenenbek on 01.04.17.
//

#ifndef CSIM2SIM_DATABASE_H
#define CSIM2SIM_DATABASE_H

//
// Created by kenenbek on 30.03.17.
//

#include <SQLiteCpp/Database.h>
#include "myfunc_list.h"
#include <string>
#include <sstream>
#include <vector>
#include <iterator>

#ifdef SQLITECPP_ENABLE_ASSERT_HANDLER
namespace SQLite
{
/// definition of the assertion handler enabled when SQLITECPP_ENABLE_ASSERT_HANDLER is defined in the project (CMakeList.txt)
void assertion_failed(const char* apFile, const long apLine, const char* apFunc, const char* apExpr, const char* apMsg)
{
    // Print a message to the standard error output stream, and abort the program.
    std::cerr << apFile << ":" << apLine << ":" << " error: assertion failed (" << apExpr << ") in " << apFunc << "() with message \"" << apMsg << "\"\n";
    std::abort();
}
}
#endif

class FileDatabase
{
public:
    // Constructor
    FileDatabase();
    ~FileDatabase();

    /// List the rows where the "weight" column is greater than the provided aParamValue
    std::map<std::string, InputFile>* GetDataInfo (std::string& CONDITION);

private:
    sqlite3* mDb;    ///< Database connection
};

class JobDatabase
{
public:
    // Constructor
    JobDatabase();
    ~JobDatabase();

    /// List the rows where the "weight" column is greater than the provided aParamValue
    std::vector<Job*>* GetJobs (std::string JOBS_AMOUNT, std::string HOST_NAME, FileDatabase& FD);

    int GetSize();

private:
    SQLite::Database    mDb;    ///< Database connection
    SQLite::Statement   mQuery; ///< Database prepared SQL query
    SQLite::Statement deleteQuery;
};

template<typename Out>
void split(const std::string &s, char delim, Out result);


std::vector<std::string> split(const std::string &text, char* sep);

std::string create_CONDITION(std::string input_output_string);
std::string prepare_condition(const std::vector<std::string>& a, const std::vector<std::string>& b);

#endif //CSIM2SIM_DATABASE_H
