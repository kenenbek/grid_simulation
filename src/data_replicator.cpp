//
// Created by ken on 28.07.16.
// Puprose: data replication
//

#include <simgrid/msg.h>
#include <string>
#include "my_structures.h"
#include "myfunc_list.h"
#include "my_msg.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(datareplica, "Messages specific for creating replicas");

int uploader(int argc, char* argv[]);
int data_replicator(int argc, char* argv[]);

int data_replicator(int argc, char* argv[]){
    /**
        @type simgrid process
        Launches `uploader` processes to replicate output files.

        Simgrid process parameters
        --------------------------
        Job* replica (job descriptor) -- contains all information about
        job parameters (type, size, etc.)
    */

    Job* replica = (Job*) MSG_process_get_data(MSG_process_self());
    size_t output_files = replica->OutputFiles.size();

    for (size_t k = 0; k < output_files; ++k) {

        InputFile* infl;
        try {
            infl = &replica->files_map->at(replica->OutputFiles.at(k));
        }catch (std::out_of_range& e){
            continue;
        }

        const std::vector<std::string>& replica_locations = infl->Storages;

        for (size_t i = 0; i < replica_locations.size(); ++i) {
            UploadData* data = new UploadData;
            data->filename = replica->OutputFiles.at(k);
            data->dest = replica_locations.at(i);

            MSG_process_create("upload", uploader, data, MSG_host_self());
        }
    }

    MSG_process_sleep(1.1);
    delete replica;

    return 0;
}

int uploader(int argc, char* argv[]){

    /**
        @type simgrid process
        Uploads output file to another host.

        Simgrid process parameters
        --------------------------
        UploadData* data -- pointer to object
        which contains inforamation about how to replicate output file.
        (destination host, filename, etc.)
    */

    std::string host_name = MSG_host_get_name(MSG_host_self());
    std::string host_short = MSG_host_get_property_value(MSG_host_self(), "shortName");
    std::string this_disk_name = host_short + "-DISK";

    std::string curFilePath;
    std::string pathAtDest;
    std::string dest_disk_name;
    std::string stor_type;

    msg_file_t file = NULL;

    UploadData* data = (UploadData*) MSG_process_get_data(MSG_process_self());

    dest_disk_name = data->dest;
    stor_type = data->dest.back();

    if (dest_disk_name.compare(this_disk_name)) {

        curFilePath = "/" + host_short + "-DISK" + data->filename;
        pathAtDest = "/" + data->dest + data->filename;

        file = MSG_file_open(curFilePath.c_str(), NULL);
        //msg_host_t dest = MSG_host_by_name(destHostName.c_str());

        if (MSG_file_get_size(file) == 0){
            XBT_INFO("%s  replica", curFilePath.c_str());
        }
        msg_error_t a = MY_MSG_file_rcopy_and_close(file, pathAtDest, data->dest);
        //cumulative_output_per_site(host_name, (double) MSG_file_get_size(file));

        if (a == MSG_OK) {
            //tracer_traffic(this_disk_name, dest_disk_name, (double) MSG_file_get_size(file));
        }  else {
            XBT_INFO("Transfer fail occurred");
        }
    }
    delete data;
    return 0;

}
