//
// Created by kenenbek on 19.04.17.
//

#include <simgrid/msg.h>
#include "myfunc_list.h"
#include "my_msg.h"


msg_file_t MY_MSG_file_open(const std::string& path){
    msg_file_t file = MSG_file_open(path.c_str(), NULL);
    file_usage_counter(path);
    return file;
}

msg_error_t MY_MSG_file_rcopy_and_close(msg_file_t& file, const std::string& dest_path, const std::string& dest_storage_name){
    msg_error_t res = MSG_file_rcopy(file, MSG_host_self(), dest_path.c_str());

    if (res == 0){
        create_file_label(dest_path);
        dataset_number_change(dest_storage_name, 1);
    }

    MSG_file_close(file);


    return res;
}