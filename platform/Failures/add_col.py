import csv

with open('capacities.txt','r') as csvinput:
    with open('capacities.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput, lineterminator='\n')
        reader = csv.reader(csvinput)

        all = []
        row = next(reader)
        row.append('HAVEDISK')
        all.append(row)

        for row in reader:
            row.append(1)
            all.append(row)

        writer.writerows(all)
